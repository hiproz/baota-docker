FROM debian:bookworm-slim

LABEL vendor="itranscloud.com"

ENV DEBIAN_FRONTEND noninteractive

COPY entrypoint.sh /
COPY bt_install.sh /
COPY need_update /

# prepare for software
RUN chmod +x /entrypoint.sh && apt-get update -qq && apt-get install -qqy --no-install-recommends yes wget procps curl iproute2 build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/*

WORKDIR /www

VOLUME [ "/www" ]

EXPOSE 8888 888 80 443 21 22 3306

ENTRYPOINT ["/entrypoint.sh"]
